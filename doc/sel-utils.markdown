# sel-utils.lsp

## BF-pickset-ssget

说明：
自定义带提示符的ssget / 参照 leemac大神

参数：

* msg - 提示符

* params - ssget参数列表

返回值: 
选择集

示例:
```
(BF-pickset-ssget "选择对象：" '("_WP" pt_list ((0 . "LINE") (62 . 5))))
 ```

## BF-pickset-getbox

说明：
选择集包围盒

参数：

* sel - 选择集

* offset - 外框偏移距离

 + 等于0 / nil，不偏移

 + 大于0，向外偏移

 + 小于0，向内偏移

返回值: 
外框（偏移后）的左下，右上角点

示例:
```
(BF-pickset-getbox sel 0.2)
 ```

## BF-pickset-ptx

说明：
取选择集4角点坐标

参数：

* sel - 选择集

* n - 角点编号

 + 左下 0 

 + 右下 1 

 + 右上 2 

 + 左上 3 

返回值: 
角点坐标

示例:
```
(BF-pickset-ptx sel 0)
 ```

## BF-pickset-sortwithdxf

说明：
选择集按照给定的组码值进行排序

参数：

* SE - 要排序的选择集

* I - 排序依据的组码号

* INT - 如果组码值为一个表，则INT指出使用第几个；否则nil

* FUZZ - 允许偏差；若无为nil

* K - T表示从大到小，nil表示从小到大返回值：

返回值: 
排序后的选择集

示例:
```
(SORT-SE SS 10 0 5.0 T)表示按照10组码的X坐标值进行排序，允许偏差值为5.0，顺序为从大到小 
 ```

```
(SORT-SE SS 10 0 5.0 T)表示按照10组码的X坐标值进行排序，允许偏差值为5.0，顺序为从大到小
 ```

```
(SORT-SE SS 8 NIL NIL NIL)表示按照8组码值（图层名称）进行排序，顺序为从小到大
 ```

## BF-pickset-sort

说明：
通用选择集，点表，图元列表排序,本程序是在fsxm的扩展 自贡黄明儒 2014年3月22日

参数：

* ssPts - 选择集，点表，图元列表

* KEY - xyzXYZ"任意组合,例如"yX",y在前表示y坐标优先，小y表示从小到大(注

* FUZZ - 允许偏差；若无为nil

返回值: 
结果根据ssPts不同，如下：

 + 选择集，返回图元列表ssPts

 + 点表(1到n维 1维时key只能是x或X)，返回点表,点表可以1到n维混合,Key长度不大于点的最小维数。

 + 图元列表，返回图元列表

示例:
```
(BF-pickset-Sort (ssget) "YxZ" 0.5);返回(<Entity name: 7ef7b3a8> <Entity name: 7ef7b3a0>)
 ```

```
(BF-pickset-Sort (list '(2 3) '(3 5)) "Yx" 0.5);返回((3 5) (2 3))
 ```

```
(BF-pickset-Sort '(<Entity name: 7ef79a28> <Entity name: 7ef79a10>) "YxZ" 0.5)
 ```

```
(BF-pickset-Sort (list "DF" "ZX" "A" "DD" "A") "X" 1)=>("ZX" "DF" "DD" "A" "A")
 ```

```
(BF-pickset-Sort (list 5 8 5 9) "X" 1)=>(9 8 5)
 ```

## BF-pickset-ssgetcrossline

说明：
取得与线相交的选择集

参数：

* ent - 线-图元名

* filter - 过滤列表

返回值: 
选择集

示例:
```
(BF-pickset-ssgetcrossline (car (entsel)) nil)
 ```

## BF-pickset->list

说明：
选择集->图元列表

参数：

* SS - 选择集

返回值: 
图元列表

示例:
```
(BF-pickset->list (ssget))
 ```

## BF-pickset->vlalist

说明：
选择集转为Vla列表

参数：

* SS - 选择集

返回值: 
Vla列表

示例:
```
(BF-pickset->vlalist (ssget))
 ```

## BF-pickset->Array

说明：
选择集->数组

参数：

* SS - 选择集

返回值: 
数组

示例:
```
(BF-pickset->Array (ssget))
 ```

## BF-pickset-Sub

说明：
选择集相减 By 自贡黄明儒2012.8.23

参数：

* SS1 - 选择集1

* SS2 - 选择集2

返回值: 
选择集 or nil

示例:
```
(BF-pickset-Sub (setq	ss1 (ssget)) (setq ss2 (ssget)))
 ```

