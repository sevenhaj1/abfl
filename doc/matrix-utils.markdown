# matrix-utils.lsp

## BF-Vec-VxS

说明：
矢量乘标量

参数：

* v - 矢量

* s - 标量

返回值: 
矢量

示例:
```
(BF-Vec-VxS '(1 2 3) 3)
 ```

## BF-Vec-Norm

说明：
矢量的模（长度）

参数：

* v - 矢量

返回值: 
长度

示例:
```
(BF-Vec-Norm '(1 2 3))
 ```

## BF-Vec-Unit

说明：
单位矢量

参数：

* v - 矢量

返回值: 
单位矢量

示例:
```
(BF-Vec-Unit '(1 2 3))
 ```

## BF-Vec-V+V

说明：
矢量相加

参数：

* v1 - 矢量1

* v2 - 矢量2

返回值: 
矢量

示例:
```
(BF-Vec-V+V '(1 2 3) '(3 2 1))
 ```

## BF-Vec-V-V

说明：
矢量相减

参数：

* v1 - 矢量1

* v2 - 矢量2

返回值: 
矢量

示例:
```
(BF-Vec-V-V '(1 2 3) '(3 2 1))
 ```

## BF-Vec-Dot

说明：
两矢量的点积 

参数：

* v1 - 矢量1

* v2 - 矢量2

返回值: 
标量

示例:
```
(BF-Vec-Dot '(1 2 3) '(3 2 1))
 ```

## BF-Vec-VxV

说明：
两矢量的叉积 

参数：

* v1 - 矢量1

* v2 - 矢量2

返回值: 
矢量

示例:
```
(BF-Vec-VxV '(1 2 3) '(3 2 1))
 ```

## BF-Mat-Trp

说明：
矩阵转置  

参数：

* m - 矩阵

返回值: 
转置后的矩阵

示例:
```
(BF-Mat-Trp '((1 2 3) (3 2 1)))
 ```

## BF-Mat-M+M

说明：
矩阵相加

参数：

* m - 矩阵1

* n - 矩阵2

返回值: 
矩阵

示例:
```
(BF-Mat-M+M '((1 3 1) (1 0 0)) '((0 0 5) (7 5 0)))
 ```

## BF-Mat-M-M

说明：
矩阵相减

参数：

* m - 矩阵1

* n - 矩阵2

返回值: 
矩阵

示例:
```
(BF-Mat-M-M '((1 3 1) (1 0 0)) '((0 0 5) (7 5 0)))
 ```

## BF-Mat-MxM

说明：
矩阵相乘

参数：

* m - 矩阵mxn

* n - 矩阵nxp

返回值: 
矩阵mxp

示例:
```
(BF-Mat-MxM '((1 0 2) (-1 3 1)) '((3 1) (2 1)(1 0)))
 ```

## BF-Mat-MxV

说明：
矢量或点的矩阵变换(矢量乘矩阵) 

参数：

* m - 矩阵nxn

* v - 矢量R^n

返回值: 
矢量R^n

示例:
```
(BF-Mat-MxV '((1 0 2) (-1 3 1) (3 12 1)) '(2 11 0))
 ```

## BF-Mat-MxP

说明：
点的矩阵(4x4 matrix) 变换

参数：

* m - 矩阵4x4

* p - 三维点

返回值: 
点变换后的位置

示例:
```
(BF-Mat-MxP '((1 0 0 1) (0 1 0 1) (0 0 1 0) (0 0 0 1)) '(2 11 0))
 ```

## BF-Mat-MxS

说明：
矩阵数乘

参数：

* m - 矩阵nxn

* s - 数

返回值: 
矩阵

示例:
```
(BF-Mat-MxS '((1 0 0 1) (0 1 0 1) (0 0 1 0) (0 0 0 1)) 2)
 ```

## BF-Mat-Translation

说明：
根据矢量计算平移矩阵

参数：

* v - 平移矢量

返回值: 
4X4的平移矩阵

示例:
```
(BF-Mat-Translation '(1 0 0))
 ```

## BF-Mat-TranslateBy2P

说明：
根据两点计算平移矩阵

参数：

* p1 - 基点

* p2 - 目标点

返回值: 
4X4的平移矩阵

示例:
```
(BF-Mat-TranslateBy2P '(1 0 0) '(2 3 0))
 ```

## BF-Mat-TranslateBymat

说明：
根据矩阵和矢量计算平移矩阵

参数：

* mat - 4X4矩阵

* p2 - 矢量

返回值: 
4X4的平移矩阵

示例:
```
(BF-Mat-TranslateBymat '((1 0 0 1) (0 1 0 1) (0 0 1 0) (0 0 0 1)) '(2 2 0))
 ```

## BF-Mat-Scaling

说明：
根据基点和缩放比例计算缩放矩阵

参数：

* Cen - 基点

* scale - 缩放比例

返回值: 
4X4的缩放矩阵

示例:
```
(BF-Mat-Scaling '(1 0 0) 2)
 ```

## BF-Mat-Rotation

说明：
根据基点和旋转角度计算旋转矩阵

参数：

* Cen - 基点

* ang - 旋转角度

返回值: 
4X4的旋转矩阵

示例:
```
(BF-Mat-Rotation '(1 0 0) (/ pi 3))
 ```

## BF-Mat-Rotation3D

说明：
根据基点、旋转轴矢量和旋转角度计算旋转矩阵

参数：

* Cen - 基点

* Axis - 旋转轴矢量

* ang - 旋转角度

返回值: 
4X4的旋转矩阵

示例:
```
(BF-Mat-Rotation3D '(1 0 0) '(1 0 0) (/ pi 3))
 ```

## BF-Mat-RotateBy2P

说明：
根据旋转轴矢量两点和旋转角度计算旋转矩阵

参数：

* p1 - 旋转轴矢量第一点

* p2 - 旋转轴矢量第二点

* ang - 旋转角度

返回值: 
4X4的旋转矩阵

示例:
```
(BF-Mat-RotateBy2P '(1 0 0) '(2 2 0) (/ pi 3))
 ```

## BF-Mat-Reflect

说明：
根据反射（镜像）轴矢量两点计算反射（镜像）矩阵

参数：

* p1 - 反射（镜像）矢量第一点

* p2 - 反射（镜像）矢量第二点

返回值: 
4X4的反射（镜像）矩阵

示例:
```
(BF-Mat-Reflect (getpoint) (getpoint))
 ```

## BF-Mat-mirroring

说明：
根据反射（镜像）平面计算反射（镜像）矩阵

参数：

* p1 - 反射（镜像）平面第一点

* p2 - 反射（镜像）平面第二点

* p3 - 反射（镜像）平面第三点

返回值: 
4X4的反射（镜像）矩阵

示例:
```
(BF-Mat-mirroring (getpoint) (getpoint) (getpoint))
 ```

## BF-Mat-DispToMatrix

说明：
把位移矢量添加到矩阵中

参数：

* mat - 3X3矩阵

* disp - 位移矢量

返回值: 
4X4的平移矩阵

示例:
```
(BF-Mat-DispToMatrix '((1 0 2) (-1 3 1) (3 12 1)) '(2 11 0))
 ```

## BF-Mat-ScaleByMatrix

说明：
点或对象的矩阵缩放变换

参数：

* target - 点表或VLA对象

* p1 - 缩放基点

* scale - 缩放比例

返回值: 
缩放后的对象或点表

示例:
```
(BF-Mat-ScaleByMatrix '((1 0 0) (0 1 0) (0 0 1) (0 0 0)) (getpoint) 2)
 ```

## BF-Mat-TranslateByMatrix

说明：
点或对象的矩阵平移变换

参数：

* target - 点表或VLA对象

* p1 - 基点

* p2 - 目标点

返回值: 
平移后的对象或点表

示例:
```
(BF-Mat-TranslateByMatrix '((1 0 0) (0 1 0) (0 0 1) (0 0 0)) (getpoint) (getpoint))
 ```

## BF-Mat-RotateByMatrix

说明：
点或对象的矩阵旋转变换

参数：

* target - 点表或VLA对象

* p1 - 基点

* ang - 旋转角度

返回值: 
旋转后的对象或点表

示例:
```
(BF-Mat-RotateByMatrix '((1 0 0) (0 1 0) (0 0 1) (0 0 0)) (getpoint) (/ pi 6))
 ```

## BF-Mat-ReflectByMatrix

说明：
点或对象的矩阵反射（镜像）变换

参数：

* target - 点表或VLA对象

* p1 - 反射（镜像）矢量第一点

* p2 - 反射（镜像）矢量第二点

返回值: 
反射（镜像）后的对象或点表

示例:
```
(BF-Mat-ReflectByMatrix '((1 0 0) (0 1 0) (0 0 1) (0 0 0)) (getpoint) (getpoint))
 ```

## BF-Mat-ApplyMatrixTransformation

说明：
点或对象的矩阵变换

参数：

* target - 点表或VLA对象

* matrix - 3x3 矩阵

* vector - 移动矢量

返回值: 
矩阵变换后的对象或点表

示例:
```

 ```

## BF-Mat-Rotate3D

说明：
点或对象的矩阵3维旋转变换

参数：

* target - 点表或VLA对象

* p1 - 旋转轴矢量的第一点

* p2 - 旋转轴矢量的第二点

* ang - 旋转角度

返回值: 
旋转后的对象或点表

示例:
```
(BF-Mat-Rotate3D '((1 0 0) (0 1 0) (0 0 1) (0 0 0)) (getpoint) (getpoint)(/ pi 6))
 ```

## BF-Mat-Reflect3D

说明：
点或对象的矩阵3维反射（镜像）变换

参数：

* target - 点表或VLA对象

* p1 - 反射（镜像）平面第一点

* p2 - 反射（镜像）平面第二点

* p3 - 反射（镜像）平面第三点

返回值: 
反射（镜像）后的对象或点表

示例:
```
(BF-Mat-Reflect3D '((1 0 0) (0 1 0) (0 0 1) (0 0 0)) (getpoint) (getpoint)(getpoint))
 ```

