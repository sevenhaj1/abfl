# curve-utils.lsp

## BF-point-3d->2d

说明：
根据三维点坐标返回二维点坐标

参数：

* 3dpt - 三维点坐标

返回值: 
二维点坐标

示例:
```
(BF-point-3d->2d	'(1 1 0))
 ```

## BF-point-2d->3d

说明：
无条件转换为3维点

参数：

* p - 点坐标或数

返回值: 
三维点坐标

示例:
```
(BF-point-2d->3d	'(1 1))
 ```

## BF-rec-2pt->4pt

说明：
根据矩形2点计算矩形4点

参数：

* pt1 - 左下点

* pt2 - 右上点

返回值: 
矩形的四个角点坐标

示例:
```
(BF-rec-2pt->4pt '(0 0 0) '(2 2 0))
 ```

## BF-curve-join

说明：
合并多段线函数

参数：

* entlst - 选择集或图元列表，vla对象列表

* fuzz - 容差值

返回值: 
合并后的多段线图元名

示例:
```
(BF-curve-join '(ent1 ent2 ent3 ..) 0.000001)
 ```

## BF-curve-inters

说明：
获取对象交点列表

参数：

* obj1 - 选择集，vla对象，图元名，vla对象表，图元表，nil

* obj2 - 选择集，vla对象，图元名，vla对象表，图元表，nil

 + obj1 和 obj2 参数可任意组合，但不能全为nil

* mode -  该参数只有obj1、obj2为图元或vla对象时，服从下列设置，其他情况均默认对象不延伸

 + acExtendNone 对象不延伸

 + acExtendThisEntity 延伸obj1

 + acExtendOtherEntity 延伸obj2

 + acExtendBoth 对象都延伸

 + nil = acExtendNone 对象不延伸

返回值: 
对象交点列表

示例:
```
(BF-curve-inters obj1 obj2 acExtendNone)
 ```

## BF-curve-putClosed

说明：
使多段线封闭By 自贡黄明儒

参数：

* obj - 多段线对象

返回值: 
无

示例:
```
(BF-curve-putClosed (car (entsel)))
 ```

## BF-curve-Pline-2dpoints

说明：
多段线端点列表，返回二维点坐标 By 自贡黄明儒

参数：

* obj - 多段线对象

返回值: 
二维点坐标列表

示例:
```
(BF-curve-Pline-2dpoints (car (entsel)))
 ```

## BF-curve-pline-3dpoints

说明：
多段线端点列表，返回三维点坐标 By 无痕

参数：

* obj - 多段线对象

返回值: 
三维点坐标列表

示例:
```
(BF-curve-pline-3dpoints (car (entsel)))
 ```

## BF-curve-Rectangle-Center

说明：
矩形中点坐标 By 自贡黄明儒

参数：

* obj - 矩形对象

返回值: 
矩形中点坐标

示例:
```
(BF-curve-Rectangle-Center (car (entsel)))
 ```

## BF-curve-Param-FirstAngle

说明：
曲线参数param处的切线方向的角度

参数：

* obj - 曲线

* param - 曲线参数值，从0开始

返回值: 
弧度制角度值

示例:
```
(BF-curve-Param-FirstAngle (car (entsel)) 0)
 ```

## BF-curve-Param-SecondAngle

说明：
曲线参数param处的法线方向的角度

参数：

* obj - 曲线

* param - 曲线参数值，从0开始

返回值: 
弧度制角度值

示例:
```
(BF-curve-Param-SecondAngle (car (entsel)) 0)
 ```

## BF-curve-Point-FirstAngle

说明：
曲线一点的切线方向的角度

参数：

* obj - 曲线

* pt - 曲线上一点的坐标

返回值: 
弧度制角度值

示例:
```
(BF-curve-Point-FirstAngle (car (entsel)) (getpoint))
 ```

## BF-curve-Point-SecondAngle

说明：
曲线一点的法线方向的角度

参数：

* obj - 曲线

* pt - 曲线上一点的坐标

返回值: 
弧度制角度值

示例:
```
(BF-curve-Point-SecondAngle (car (entsel)) (getpoint))
 ```

## BF-curve-PtOnCurve

说明：
判断点是否在曲线上

参数：

* curve - 曲线

* pt - 点的坐标

返回值: 
在曲线上返回T，反之nil

示例:
```
(BF-curve-PtOnCurve (getpoint) (car (entsel)))
 ```

## BF-curve-Length

说明：
曲线长度

参数：

* curve - 曲线，直线、圆弧、圆、多段线、优化多段线、样条曲线等图元

返回值: 
曲线的长度

示例:
```
(BF-curve-Length (car (entsel)))
 ```

## BF-curve-subsegments

说明：
多段线子段数量

参数：

* curve - 多段线

返回值: 
子段的数量

示例:
```
(BF-curve-subsegments (car (entsel)))
 ```

## BF-curve-Midpoint

说明：
曲线中点

参数：

* curve - 曲线

返回值: 
中点坐标

示例:
```
(BF-curve-Midpoint (car (entsel)))
 ```

## BF-curve-subsegment-points

说明：
多段线第n子段的端点坐标

参数：

* curve - 多段线

* n - 第n个子段

返回值: 
子段的端点坐标列表

示例:
```
(BF-curve-subsegment-points (car (entsel)) 2)
 ```

## BF-curve-picked-subsegment-Points

说明：
多段线所点击子段的两端点列表

参数：

* obj - 多段线

* p - 点击点

返回值: 
点击子段的端点坐标列表

示例:
```
(BF-curve-picked-subsegment-Points (car(setq en(entsel))) (cadr en))
 ```

## BF-curve-PickClosePointto

说明：
多段线上距离点击点最近的一个顶点  By 自贡黄明儒

参数：

* obj - 多段线

* p - 点击点

返回值: 
顶点坐标

示例:
```
(BF-curve-PickClosePointto (car(setq en(entsel))) (cadr en))
 ```

## BF-curve-subsegment-picked-param

说明：
多段线所点击子段参数  By 自贡黄明儒

参数：

* obj - 多段线

* p - 点击点

返回值: 
子段的参数

示例:
```
(BF-curve-subsegment-picked-param (car(setq en(entsel))) (cadr en))
 ```

## BF-curve-subsegment-length

说明：
多段线子段长度  By 自贡黄明儒

参数：

* obj - 多段线

* pt1 - 子段的起点坐标或参数

* pt1 - 子段的终点坐标或参数

返回值: 
子段的长度

示例:
```
(BF-curve-subsegment-length (car(entsel)) 0 1)
 ```

## BF-curve-subsegment-Picked-type

说明：
多段线子段图元类型

参数：

* obj - 多段线

* p - 点击子段的坐标或参数

返回值: 
表示子段图元类型的字符串，直线为line，圆弧为arc

示例:
```
(BF-curve-subsegment-Picked-type (car(entsel)) 0)
 ```

```
(BF-curve-subsegment-Picked-type (car(setq en(entsel))) (cadr en))
 ```

## BF-curve-checkarc

说明：
判断多段线是否有圆弧(凸度/=0)的子段

参数：

* en - 多段线图元名

返回值: 
有圆弧返回t，反之nil

示例:
```
(BF-curve-checkarc (car(entsel)))
 ```

## BF-curve-subsegment-parameter

说明：
多段线子段参数

参数：

* curve - 多段线图元名

* pt - 点击点

返回值: 
根据子段类型不同返回不同的参数

 + 直线：起点，终点，长度，角度

 + 弧：圆心，起点，终点，切线交点，半径，包含角，弧长，切线交点长

示例:
```
(BF-curve-subsegment-parameter (car(setq en(entsel))) (cadr en))
 ```

