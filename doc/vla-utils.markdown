# vla-utils.lsp

## BF-vla-getvalue

说明：
变体里取值

参数：

* var - 变体或者数组

返回值: 
数据列表

示例:
```
(BF-vla-getvalue	var)
 ```

## BF-vla-List->Array

说明：
表->安全数组类型（一维数组）

参数：

* nlist - 列表，要求数据的类型要和arraytype一致

* arraytype - 可指定如下常量：可以用后面的数字也可以用前面的类型符号

 + vlax-vbInteger (2) 整数型

 + vlax-vbLong (3) 长整数型

 + vlax-vbSingle (4) 单精度浮点数

 + vlax-vbDouble (5) 双精度浮点数

 + vlax-vbString (8) 字符串

 + vlax-vbObject (9) 对象

 + vlax-vbBoolean (11) 布尔值

 + vlax-vbVariant (12) 变体

返回值: 
一维数组

示例:
```
(BF-vla-List->Array '(1 2 3 4) vlax-vbInteger)
 ```

## BF-vla-list->arrays

说明：
表->安全数组类型（多维数组，多于二维报错）

参数：

* nlist - 列表，要求数据的类型要和arraytype一致，表的各维必须为表

* arraytype - 可指定如下常量：可以用后面的数字也可以用前面的类型符号

 + vlax-vbInteger (2) 整数型

 + vlax-vbLong (3) 长整数型

 + vlax-vbSingle (4) 单精度浮点数

 + vlax-vbDouble (5) 双精度浮点数

 + vlax-vbString (8) 字符串

 + vlax-vbObject (9) 对象

 + vlax-vbBoolean (11) 布尔值

 + vlax-vbVariant (12) 变体

返回值: 
多维数组

示例:
```
(BF-vla-List->Arrays '((1 2) (3 4)) vlax-vbInteger)
 ```

## BF-vla-ObjectVariant

说明：
创建vla对象表变体

参数：

* lst - vla对象表

返回值: 
变体

示例:
```
(BF-vla-ObjectVariant lst)
 ```

## BF-enamelist->vla

说明：
图元列表转为Vla列表

参数：

* lst - 图元列表

返回值: 
Vla列表

示例:
```
(BF-enamelist->vla lst)
 ```

