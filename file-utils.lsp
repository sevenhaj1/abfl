;;;name:BF-File-ReadToList
;;;desc:读取文件生成表
;;;arg:filepath:文件路径
;;;return:返回生成的字符串表
;;;example:(BF-File-ReadToList "E:\\tmp.lsp")
(defun BF-File-ReadToList (filepath / fn tmplst value)
  (if (setq filepath (findfile filepath))
    (progn
      (setq fn (open filepath "r"))
      (while (setq value (read-line fn))
	(setq tmplst (cons value tmplst))
      )
      (close fn)
      (reverse tmplst)
    )
    nil
  )
)

;;;name:BF-File-write
;;;desc:将字符串列表写入文件
;;;arg:filepath:文件路径
;;;arg:lst:字符串列表
;;;return:
;;;example:(BF-File-write "E:\\tmp.lsp" '("1" "2" "3"))
(defun BF-File-write (filepath lst / fn)
  (if (setq filepath (findfile filepath))
    (write-line
      (BF-lst->str lst "\n")
      (setq fn (open filepath "w"))
    )
  )
  (close fn)
)




;;;name:BF-File-ChangeLine
;;;desc:修改文件指定行
;;;arg:filepath:文件路径
;;;arg:n:行数
;;;arg:value:需要替换的字符串
;;;return:无
;;;example:(BF-File-ChangeLine "E:\\tmp.lsp" 2 "try this func")
(defun BF-File-ChangeLine (filepath n value / fn tmplist)
  (setq	tmplist	(BF-list-subst
		  (1- n)
		  value
		  (setq tmplist (BF-File-ReadToList filepath))
		)
  )
  (write-line
    (BF-lst->str tmplist "\n")
    (setq fn (open filepath "w"))
  )
  (close fn)
)

;;;name:BF-File-Length
;;;desc:计算文件的行数
;;;arg:files:文件地址,getfiled函数返回的格式
;;;return:文件的行数
;;;example:(BF-File-Length "E:\\tmp.lsp")
(defun BF-File-Length (files / tmplst x fn)
  (setq files (findfile files))
  (if files
    (progn (setq tmplst 0)
	   (setq fn (open files "r"))
	   (while (read-line fn) (setq tmplst (+ 1 tmplst)))
	   (close fn)
	   tmplst
    )
    nil
  )
)
				
;;;name:BF-File-removelines
;;;desc:删除文件的指定行
;;;arg:name:文件路径
;;;arg:a:行或行列表，从0开始
;;;return:
;;;example:(BF-File-write "E:\\tmp.lsp" '(1 4))
(defun BF-File-removelines (name a / i lst)
  (or (listp a) (setq a (list a)))
  (setq	i   -1
	lst (BF-File-ReadToList name)
  )
  (setq	lst (vl-remove-if
	      '(lambda (x) (setq i (1+ i)) (member i a))
	      lst
	    )
  )
  (BF-File-write name lst)
)

;;;name:BF-File-append
;;;desc:追加文件内容
;;;arg:name:文件路径
;;;arg:lst:字符串或字符串表
;;;return:
;;;example:(BF-File-write "E:\\tmp.lsp" '("1" "4"))
(defun BF-File-append (name lst / ff)
  (or (listp lst) (setq lst (list lst)))
  (write-line
    (BF-lst->str lst "\n")
    (setq ff (open name "a"))
  )
  (close ff)
)

;;;name:BF-File-moveline
;;;desc:文件某行上（下）移指定行
;;;arg:name:文件路径
;;;arg:a:要位移的行
;;;arg:kk:位移行数，正为下移，负为上移
;;;return:
;;;example:(BF-File-write "E:\\tmp.lsp" '("1" "4"))
(defun BF-File-moveline	(name a k / item lst)
  (setq lst (BF-File-readtolist name))
  (setq item (nth a lst))
  (setq lst (BF-List-removeindex lst a))
  (BF-File-write
    name
    (BF-List-insert lst (+ a k) item)
  )
)


