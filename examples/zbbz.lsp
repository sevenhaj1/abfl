;|
这是一个完整的功能程序，实现坐标标注的功能。
本程序为BF函数的一个示例程序。主要用来演示dcl函数。
本程序中专用子函数的命名规则为：
BF-APP-BZB-XXX
BF——一级前缀，说明程序所属的组织/个人。
APP——二级前缀，说明是程序独有的子函数。
BZB——三级前缀，说明是哪个程序来使用这个函数。
XXX——函数本名，说明函数的功能。
|;

;;;函数名称:bf-app-bzb-excel
;;;函数说明:输出坐标到excel
;;;参    数:lst:要输出的坐标表
;;;返 回 值:
;;;示    例:(bf-app-bzb-excel lst)
(defun bf-app-bzb-excel (lst / i)
	(setq excelobj (BF-Excel-New t))
	(BF-Excel-MergeRange excelobj '(1 1 1 4))
	(BF-Excel-setRangeValue excelobj '(1 1) "坐标表")
	(BF-Excel-setRangeValue excelobj '(2 1 2 4) '("序号" "X" "Y" "Z"))
	(setq i 3)
	
	(foreach item lst 
		(BF-Excel-setRangeValue excelobj (list i 1 i 4) (cons (- i 2) item))
		(setq i (1+ i))
	)
)


;;;函数名称:BF-APP-bzb-table
;;;函数说明:输出列表到cad表格
;;;参    数:pt:表格插入点
;;;参    数:lst:要输出的表
;;;返 回 值:
;;;示    例:(BF-APP-bzb-table pt lst)
(defun BF-App-bzb-table (pt lst / i j len tab)
	(setq len (length lst))
	(setq tab (vla-AddTable (BF-model-space) (vlax-3D-point pt) (+ 2 len) 4 15 50))
	(setq i 0)
	(if	vla-put-RegenerateTableSuppressed
		(vla-put-RegenerateTableSuppressed tab :vlax-false)
	)
	(vla-SetText tab 0 0 "坐标表")
	(mapcar '(lambda (x) (vla-SetText tab 1 i x)(setq i (1+ i)))
		'("序号" "X" "Y" "Z")
	)
	(setq i 2)
	(foreach item lst 
		(setq j 0)
		(mapcar
			'(lambda (a)
				 (if (< j 4)
					 (vla-settext tab i j a)
				 )
				 (setq j (1+ j))
			 )
			(cons (- i 1) item)
		)
		(setq i (1+ i))
	)
	
	(if	vla-put-RegenerateTableSuppressed
		(vla-put-RegenerateTableSuppressed tab :vlax-true)
	)
)






;;;函数名称:bf-app-bzb-drawdcl
;;;函数说明:dcl图形操作界面函数
;;;返 回 值:图形界面里的控件值列表
;;;示    例:(bf-app-bzb-drawdcl )
(defun bf-app-bzb-drawdcl (/ baseang basept bf-app-bzb-getvalue dcl-id result tmp what_next)
	(setq dcl-id  ;用于存储bf-dcl-init函数的返回值
		;;调用bf-dcl-init函数，三个参数分别为对话框的标题，默认焦点控件的key，控件列表
		(BF-dcl-Init "坐标标注设置" "celiang-coord"   
			;;控件列表的组成为：
			;;   (list
			;;			(控件布局
			;;				(控件1 控件2 控件3 ...))
			;;			(控件布局		
			;;				(控件4 控件5 控件6 ...)))
			;;	控件的布局可以嵌套布局
			(list  
				;;本程序的第一个布局为row
				;;使用bf-dcl-setlayout函数来生成布局，第一个参数为布局类型，这里为row
				;;第二个参数为布局的属性表，这里为nil，即不设置属性
				;;第三个参数为布局里控件列表，包括布局。
				(BF-dcl-setLayout "row" nil
					;;;这里为第三个参数的定义之处，由于最外层布局里还有三个二级布局，因此这里用list
					(list
						;;定义二级布局，为boxed_row
						;;其中第二个参数为属性表，形式为 ((label . "坐标类型")(width . 100) ...)，属性名字不用加引号。 
						(BF-dcl-setLayout "boxed_row" 
							'((label . "坐标类型")) 
							;;同95行，第三个参数为两个三级布局
							(list 
								(BF-dcl-setLayout "column" nil ;同上面解释为布局定义
									;;此处为三级布局里的两个具体控件radio_button
									(list 
										;;控件定义函数为BF-dcl-addItem
										;;该函数第一个参数为控件的名字，需要加引号
										;;该函数的第二个参数为控件的属性表，同97行解释
										(BF-dcl-addItem "radio_button" '((key . "math-coord")(label . "数学坐标")))
										(BF-dcl-addItem "radio_button" '((key . "celiang-coord")(label . "测量坐标")))
									);控件表结束
								);三级布局结束
								;;;同上一个同级布局解释
								(BF-dcl-setLayout "column" nil
									(list 
										(BF-dcl-addItem "text" '((key . "coord-type-up")(label . "A=Y")(alignment . centered)))
										(BF-dcl-addItem "text" '((label . "----")(alignment . centered)))
										(BF-dcl-addItem "text" '((key . "coord-type-down")(label . "B=X")(alignment . centered)))
									)
								)
							)
						);二级布局结束
						;;二级布局开始，解释同上
						(BF-dcl-setLayout "boxed_radio_column" 
							'((label . "坐标取值")) 
							(list 
								(BF-dcl-addItem "radio_button" '((key . "world-coord")(label . "世界坐标")))
								(BF-dcl-addItem "radio_button" '((key . "user-coord")(label . "用户坐标")))
							)
						)
						;;同上
						(BF-dcl-setLayout "boxed_column" 
							'((label . "当前基点坐标")(width . 30)) 
							(list 
								(BF-dcl-addItem "text" '((key . "basetext")(alignment . centered)(label . "(X=0.000,Y=0.000)")))
								(BF-dcl-setLayout "row"
									nil
									(list 
										(BF-dcl-addItem "toggle" '((key . "tog")(label . "重新定义")))
										(BF-dcl-addItem "button" '((key . "basebtn")(label . "指定基点")))
									)
								)
							)
						)
					)
				);一级布局结束
				;;一级布局开始，同上
				(BF-dcl-setLayout "row" nil
					(list 
						(BF-dcl-setLayout "boxed_row" '((label . "文字外观"))
							(list 
								(BF-dcl-setLayout "column" nil
									(list 
										(BF-dcl-addItem "edit_box" '((key . "theight")(label . "文字字高:")(edit_width . 11)(alignment . left)))
										(BF-dcl-addItem "edit_box" '((key . "twidth")(label . "文字字宽:")(edit_width . 11)(alignment . left)))
										(BF-dcl-addItem "edit_box" '((key . "prefix-x")(label . "横标前缀:")(edit_width . 11)(alignment . left)))
										(BF-dcl-addItem "edit_box" '((key . "prefix-y")(label . "纵标前缀:")(edit_width . 11)(alignment . left)))	
									)
								)
								(BF-dcl-setLayout "column" nil
									(list 
										(BF-dcl-addItem "popup_list" '((key . "tstyle")(label . "文字样式:")(edit_width . 10)(alignment . right)))
										(BF-dcl-addItem "popup_list" '((key . "tlayer")(label . "文字图层:")(edit_width . 10)(alignment . right)))
										(BF-dcl-addItem "popup_list" '((key . "arrow")(label . "箭头样式:")(edit_width . 10)(alignment . right)))
										(BF-dcl-addItem "popup_list" '((key . "jingdu")(label . "标注精度:")(edit_width . 10)(alignment . right)))
									)
								)
							)
						)
						(BF-dcl-setLayout "boxed_column" '((label . "自定义坐标系"))
							(list 
								(BF-dcl-addItem "edit_box" '((key . "base-x")(label . "基点横向坐标:")(edit_width . 11)))
								(BF-dcl-addItem "edit_box" '((key . "base-y")(label . "基点纵向坐标:")(edit_width . 11)))
								(BF-dcl-addItem "edit_box" '((key . "coord-ro")(label . "坐标网旋转角:")(edit_width . 11)))
								(BF-dcl-addItem "button" '((key . "pick-ang")(label . "拾取角度")))
							)
						)
					)
				)
				;;最后加上确认取消按钮
				(list "ok_cancel;")
			)
		);bf-dcl-init函数结束，函数会自动根据控件列表生成对话框文件，并加载，执行load_dialog，并返回dcl-id
	)
	;;定义一个内部函数用于取出每个key的值
	(defun bf-app-bzb-getvalue (/ keys)
		(setq keys '("math-coord" "celiang-coord" "world-coord" "user-coord" "tog" "theight" "twidth" "prefix-x" "prefix-y" "tstyle" "tlayer" "arrow" "jingdu" "base-x" "base-y" "coord-ro"))
		(mapcar 'cons keys (BF-dcl-getValues keys))
	)
	(setq result '());保存对话框最终返回值的变量
	(setq what_next 2);设置一个变量用于保存start_dialog函数返回的状态码
	;;如果状态码大于等于2就一直循环调用bf-dcl-start函数，其实就是调用对话框的action_tile，mode_tile等及start_dialog
	;;这种技巧用来做隐藏对话框，在执行完取点，获取输入后再次显示对话框
  (while (>= what_next 2)		
    (setq what_next ;保持状态码
			;;调用bf-dcl-start函数
			;;第一个参数为bf-dcl-init函数的返回值dcl-id点对表
			;;第二个参数为需要在load_dialog之后，start_dialog之前执行的设置属性，设置控件动作的列表。该列表的组织形式如下：
			;;(
			;;	("set" ("ket" . "1")("qer" . "2")) ;表示为给对话框各控件设置的初始值，以set表示，后面跟着 key-值 的点对表
			;;	("mode" ("ket" . 1)("qer" . 0)) ; 表示为给对话框各控件设置的显示模式，以mode表示，后面跟着 key-mode 的点对表
			;;	("action" ("buttonkey" "(func args) (func1 arg1 arg2) (done_dialog 4)") ("buttonkey1" "(func args) (func1 arg1 arg2) (done_dialog 2)")) ; 表示为给对话框各控件设置的控件被触发后要执行的动作和表达式，以action表示，后面跟着 key-表达式的表
			;;	("list" ("lst1" (1 2 3 4)) ("lst3" (1 2 3 4)));表示给list_box，popup_list控件设置的列表，以list表示，后面跟着key-值表的表
			;;	("image" ("img1" 2)("img2" 3));表示给image和image_button控件设置的幻灯片，以image表示，后面跟着key-sld的表
			;;)
			(BF-dcl-Start 
				dcl-id
				(list 
					'("set" ;表示为给对话框设置初始值
						 
						 ;;表示为key = jingdu的这个控件设置初始值，因为本程序为循环显示对话框，
						 ;;并且前一次对话框的一些设置的值要在下一次显示时自动设置，不用二次设置，
						 ;;因此每次对话框结束时都要存储对话框各控件值到result变量里，
						 ;;这里判断变量里是否有该key的值，有则设置，没有就默认为 3 。
						 ;;这里直接写正规的lisp表达式就可以，程序会自动处理成set_tile认识的样子。
						 ("jingdu" . (if (setq tmp (BF-AssocList-Key result "jingdu")) tmp "3"))
						 ("arrow" . (if (setq tmp (BF-AssocList-Key result "arrow")) tmp  "1"));同上
						 ("prefix-x" . (if (setq tmp (BF-AssocList-Key result "prefix-x")) tmp "X="))
						 ("prefix-y" . (if (setq tmp (BF-AssocList-Key result "prefix-y")) tmp "Y="))
						 ("theight" . (if (setq tmp (BF-AssocList-Key result "theight")) tmp  "3"))
						 ("twidth" . (if (setq tmp (BF-AssocList-Key result "twidth")) tmp  "0.8"))
						 ("tog" . (if (setq tmp (BF-AssocList-Key result "tog")) tmp  "0"))
						 ("tlayer" . (if (setq tmp (BF-AssocList-Key result "tlayer")) tmp  "0"))
						 ("tstyle" . (if (setq tmp (BF-AssocList-Key result "tstyle")) tmp  "0"))
						 ("celiang-coord" . (if (setq tmp (BF-AssocList-Key result "celiang-coord")) tmp "1"))
						 ("user-coord" . (if (setq tmp (BF-AssocList-Key result "user-coord")) tmp "1"))
						 ("base-x" . (if (setq tmp (BF-AssocList-Key result "base-x")) tmp ""))
						 ("base-y" . (if (setq tmp (BF-AssocList-Key result "base-y"))  tmp ""))
						 ("coord-ro" . (if (setq tmp (BF-AssocList-Key result "pick-ang")) (BF-math-rtos tmp 3)  ""))
						 ("basetext" . (if (setq tmp (BF-AssocList-Key result "basebtn")) 
														 (strcat "(X=" (BF-math-rtos (car tmp) 3) ",Y=" (BF-math-rtos (cadr tmp) 3) ")") 
														 "(X=0.000,Y=0.000)"))
					 )
					'("mode" ;表示为给对话框设置控件的显示模式
						 ("base-x" . (if (= "1" (BF-AssocList-Key result "tog")) 0 1));解释同上
						 ("base-y" . (if (= "1" (BF-AssocList-Key result "tog")) 0 1))
						 ("coord-ro" . (if (= "1" (BF-AssocList-Key result "tog")) 0 1))
						 ("pick-ang" . (if (= "1" (BF-AssocList-Key result "tog")) 0 1))
						 ("basebtn" . (if (= "1" (BF-AssocList-Key result "tog")) 0 1))
					 )
					'("list" ;表示给list_box，popup_list控件设置的列表
						 ("jingdu" ("[0]0" "[1]0.0" "[2]0.00" "[3]0.000"))
						 ("arrow" ("无" "十字" "箭头" "圆点"))
						 ("tlayer" (BF-ent-Layers))
						 ("tstyle" (BF-Ent-TextStyles))
					 )
					'("action" ;表示给对话框控件设置动作表达式
						 ;;为key = world-coord 的控件设置该控件被触发时要执行的表达式。
						 ;;这个表达式也写成正规的lisp表达式，不用写成action_tile函数认识的字符串，程序会做自动的转换
						 ("world-coord" 
							 ;;这里要完成的功能是，在world-coord控件被触发时，将下面几个控件的显示模式定义为无法作用
							 ;;BF-dcl-setModes函数为一次性的设置多个key的显示模式
							 (BF-dcl-setModes '(("base-x" . 1)("base-y" . 1)("coord-ro" . 1)("pick-ang" . 1)("basebtn" . 1)("tog" . 1))))
						 ("user-coord" 
							 (mode_tile "tog" 0) ;user-coord触发时，首先将tog设置为可以作用模式
							 (if (= "1" (get_tile "tog"));然后判断这个控件的值是否表示被选中，本示例的tog为一个复选框
								 ;;如果被选中就设置下列key的显示模式为可以作用
								 (BF-dcl-setModes '(("base-x" . 0)("base-y" . 0)("coord-ro" . 0)("pick-ang" . 0)("basebtn" . 0)))
								 ;;如果未选中，就设置下列key的显示模式为不可作用
								 (BF-dcl-setModes '(("base-x" . 1)("base-y" . 1)("coord-ro" . 1)("pick-ang" . 1)("basebtn" . 1))))
						 )
						 ;;设置tog的动作，如果选中就设置下列key的显示模式为可以作用，否则为不可作用
						 ("tog" (if (= "1" (get_tile "tog"))
											(BF-dcl-setModes '(("base-x" . 0)("base-y" . 0)("coord-ro" . 0)("pick-ang" . 0)("basebtn" . 0)))
											(BF-dcl-setModes '(("base-x" . 1)("base-y" . 1)("coord-ro" . 1)("pick-ang" . 1)("basebtn" . 1)))))
						 ;;设置当math-coord被触发时，设置下列控件的值
						 ("math-coord" 
							 ;;BF-dcl-setValues函数为一次性设置多个控件值
							 (BF-dcl-setValues '(("coord-type-up" . "X=X")("coord-type-down" . "Y=Y"))))
						 ("celiang-coord" 
							 (BF-dcl-setValues '(("coord-type-up" . "A=Y")("coord-type-down" . "B=X"))))
						 ;;设置按钮basebtn的动作为首先获取对话框各控件的值，保存到result变量，并调用done_dialog函数，状态码为4
						 ("basebtn" (setq result (BF-AssocList-AppendList result (bf-app-bzb-getvalue)))(done_dialog 4))
						 ("pick-ang" (setq result (BF-AssocList-AppendList result (bf-app-bzb-getvalue)))(done_dialog 5));同上，状态码为5
						 ("accept" (setq result (BF-AssocList-AppendList result (bf-app-bzb-getvalue)))(done_dialog 1));设置确认按钮，状态码为1
					 )	
				)
			));bf-dcl-start函数结束
    (cond ;根据返回的状态码，来执行不同的操作
      ((= what_next 4) 
				;;状态码等于4时，首先获取一个基点，然后将结果存储到result变量
				(setq basept (getpoint "\n选择基点:"))
				(setq result (BF-AssocList-AppendItem  result (cons "basebtn" basept)))
      )
			((= what_next 5)
				;;状态码为5时，获取角度，并存入result变量
				(setq baseang (getorient "\n选择坐标系旋转角度(以横轴逆时针旋转角度):"))
				(setq result (BF-AssocList-AppendItem  result (cons "pick-ang" baseang)))
      )
      ((= what_next 0)
				(prompt "\n用户取消对话框！")
      )
    );cond结束
  );循环结束，表示如果状态码为0 1 或者小于0时，就退出循环，如果大于等于2，本程序为4 或者5，就循环显示对话框->取点/角度->显示对话框...
	(BF-dcl-End dcl-id);最后结束对话框，清理生成的dcl文件，情况存储dcl-id的全局变量
	result ;最后返回result的结果。
)
;原坐标标注代码 wzg356 2014.9.12
;现坐标标注代码 vicwjb 2015.5.21
(defun c:bzb (/ *error* *olderror* accaction ang basepoint dcl-id hzt layout1 newerr pickaction pickbutton startpoint sysvarlst x-edit y-edit z-edit)
  ;自定义新的出错函数
	(defun newerr (msg)
		(mapcar 'eval sysvarlst);恢复变量设置
		(if *olderror* (setq *error* *olderror*  *olderror* nil)) ;_ 恢复*error*函数
		(setq shuju nil);清除数据
		
		(if (not (member msg '(nil "函数被取消" ";错误:quit / exit abort")))
			(princ (strcat ";错误:" msg))
    )
  )
  ;;系统设置
	(BF-startundo (BF-active-document));;命令编组开始
  (setq sysvarlst (mapcar (function (lambda (n) (list 'setvar n (getvar n))))
										'( "osmode" "cmdecho" "OSNAPCOORD" "dimzin" "plinewid" "TEXTSIZE" "textstyle")));保存系统变量
  (setq *olderror* *error*);保存出错函数
  (setq  *error* newerr);设置自定义出错函数  
  (setvar "cmdecho" 0);;;关闭命令响应
  (setvar "OSNAPCOORD" 1);;;坐标数据优先级设为：键盘输入替代对象捕捉设置
  (setvar "OSMODE" 679);;;改变捕捉模式
  (setvar "dimzin" 0);;;不对主单位值作消零处理
	(setq shuju '());;;设置存储数据的变量
	;=========================
	;===========================
  ;开始标注
  (bf-app-bzb-sub)
  ;;恢复设置
	(BF-endundo (BF-active-document));;活动编组结束
  (mapcar 'eval sysvarlst);恢复变量设置
  (setq *error* *olderror*);;恢复出错函数
	
	(princ)  
);defun
;;;函数名称:bf-app-bzb-addtext
;;;函数说明:画文字
;;;参    数:textstr:文字字符串
;;;参    数:insertpt:插入点
;;;参    数:height:字高
;;;参    数:property:文字特性值表，如((Layer . "0") (StyleName . "songti") (ScaleFactor . 0.8) (TextAlignmentPoint 1 1 0) (Alignment . 2))
;;;返 回 值:文字对象
;;;示    例:(bf-app-bzb-addtext textstr insertpt height property)
(defun bf-app-bzb-addtext (textstr insertpt height property / textobj)
	(setq textobj (vla-AddText (BF-model-space) textstr (vlax-3D-point insertpt) height))
	(foreach item property
		((eval (read (strcat "vla-put-" (vl-symbol-name (car item))))) textobj (cdr item))
	)
	textobj
)

;;;函数名称:bf-app-bzb-shizi
;;;函数说明:画十字函数
;;;参    数:pt:十字中点
;;;参    数:size:十字大小
;;;返 回 值:组成十字的两条直线的对象
;;;示    例:(bf-app-bzb-shizi pt size)
(defun bf-app-bzb-shizi (pt size)
	(list 
		(BF-ent-line (polar pt0 0 size) (polar pt0 pi size))
		(BF-ent-line (polar pt0 (* pi 0.5) size) (polar pt0 (* pi -0.5) size)))
)
;;;函数名称:bf-app-bzb-arrow
;;;函数说明:画箭头
;;;参    数:start:箭头起点
;;;参    数:end:箭头终点
;;;返 回 值:表示箭头的多段线对象
;;;示    例:(bf-app-bzb-arrow (getpoint) (getpoint))
(defun bf-app-bzb-arrow (start end size / arrobj len mid)
	(setq len (BF-Math-Length start end))
	(if (< size len)
		(setq mid (polar start (angle start end) size))
		(setq mid end)
	)
	(setq arrobj (vla-AddLightweightPolyline 
								 (BF-model-space) 
								 (BF-vla-List->Array 
									 (apply 'append (mapcar 'BF-point-3d->2d (list start mid end))) 5)))
	(vla-SetWidth arrobj 0 0 (/ size 3))
	arrobj
)
;;;函数名称:bf-app-bzb-dot
;;;函数说明:画实心圆点
;;;参    数:pt:圆心
;;;返 回 值:圆点对象
;;;示    例:(bf-app-bzb-dot (getpoint))
(defun bf-app-bzb-dot (pt size / end start yuanobj)
	(setq size (/ size 6))
	(setq start (polar pt 0 size))
	(setq end (polar pt pi size))
	(setq yuanobj (vla-AddLightweightPolyline 
									(BF-model-space) 
									(BF-vla-List->Array 
										(apply 'append (mapcar 'BF-point-3d->2d (list start end))) 5)))
	(vla-put-Closed yuanobj :vlax-true)
	(vla-SetBulge yuanobj 0 1)
	(vla-SetBulge yuanobj 1 1)
	(vla-SetWidth yuanobj 0 size size)
	(vla-SetWidth yuanobj 1 size size)
	(vla-put-ConstantWidth yuanobj (* 2 size))
	
	
	yuanobj
)

;;;函数名称:bf-app-bzb-sub
;;;函数说明:标注子函数
;;;返 回 值:无
;;;示    例:(bf-app-bzb-sub )
(defun bf-app-bzb-sub (/ arrow basebtn base-x base-y celiang-coord coord-ro gr gr-model gr-value hengxian jingdu p1 p2 pick-ang prefix-x prefix-y pt0 pt2 shangtext shizi theight tlayer tpt tstyle twidth user-coord xianchang xiatext xiexian xpt ypt)
	(initget "S T E  ")  ;带空格的关键字
	(setq pt0 (getpoint "\n点取标注点或[设置(S)/生成CAD表格(T)/输出坐标Excel(E)]:"));这样可在不需设置时直接进入标注
	
	(cond
		((= pt0 "") nil);右键、空格退出
		((= pt0 "S") 
			(setq option (bf-app-bzb-drawdcl))   
      (bf-app-bzb-sub);设置完后循环至取标注点，有递归的影子
    )
		((= pt0 "T") (BF-APP-bzb-table (getpoint "\n指定表格插入点:") (reverse shuju)) (bf-app-bzb-sub));输出坐标表
    ((= pt0 "E") (bf-app-bzb-excel (reverse shuju))(bf-app-bzb-sub));输出坐标表
		((= 'list (type pt0)) 
			;;设置基础参数
			(if option
				(progn
					(setq 
						celiang-coord (BF-AssocList-Key option "celiang-coord")
						user-coord (BF-AssocList-Key option "user-coord")
						tog (BF-AssocList-Key option "tog")
						theight (atof (BF-AssocList-Key option "theight"))
						twidth (atof (BF-AssocList-Key option "twidth"))
						prefix-x (BF-AssocList-Key option "prefix-x")
						prefix-y (BF-AssocList-Key option "prefix-y")
						tstyle (nth (atoi (BF-AssocList-Key option "tstyle")) (BF-Ent-TextStyles))
						tlayer (nth (atoi (BF-AssocList-Key option "tlayer")) (BF-ent-Layers))
						arrow (BF-AssocList-Key option "arrow")
						jingdu (atoi(BF-AssocList-Key option "jingdu"))
						base-x (if (= "" (setq base-x (BF-AssocList-Key option "base-x"))) (car pt0) (atof base-x))
						base-y (if (= "" (setq base-y (BF-AssocList-Key option "base-y"))) (cadr pt0) (atof base-y))
						pick-ang (if (= "0" tog) 0.0 (BF-AssocList-Key option "pick-ang"))
						basebtn (cond 
											((and (= "0" celiang-coord)(= "0" user-coord)) pt0) ;数学，世界
											((and (= "0" celiang-coord)(= "1" user-coord)) (if (= "0" tog) pt0 (BF-AssocList-Key option "basebtn"))) ;数学，用户
											((and (= "1" celiang-coord)(= "0" user-coord)) pt0) ;测量，世界
											((and (= "1" celiang-coord)(= "1" user-coord)) (if (= "0" tog) pt0 (BF-AssocList-Key option "basebtn"))) ;测量，用户
										)
					))
				(setq 
					celiang-coord "0"
					user-coord "0"
					tog "0"
					theight 3
					twidth 0.8
					prefix-x "X="
					prefix-y "Y="
					tstyle 	(vla-get-name (vla-get-ActiveTextStyle (BF-active-document)))
					tlayer 	(vla-get-name (vla-get-ActiveLayer (BF-active-document)))
					arrow "1"
					jingdu 3
					base-x (car pt0)
					base-y (cadr pt0)
					pick-ang 0.0
					basebtn pt0
				)
			)
			(setq tpt (BF-math-TransPt basebtn (list base-x base-y 0.0) pt0 pick-ang));计算用户坐标
			;;计算文字插入点
			(setq p1 (polar pt0 (* pi 0.5) (/ theight 3)));上文字位置
			(setq p2 (polar pt0 (* pi  -0.5) (+ theight (/ theight 3))));下文字位置 
			
			(if (= "0" celiang-coord) 
				(setq 
					xpt (strcat prefix-x (rtos (car tpt) 2 jingdu)) ;文字内容 
					ypt (strcat prefix-y (rtos (cadr tpt) 2 jingdu))
				)
				(setq 
					xpt (strcat prefix-x (rtos (cadr tpt) 2 jingdu)) ;文字内容 
					ypt (strcat prefix-y (rtos (car tpt) 2 jingdu))
				)
			)
			(setvar "osmode" 0)
			(setq ;生成文字
				shangtext (bf-app-bzb-addtext xpt p1 theight 
										(list 
											(cons 'Layer tlayer)
											(cons 'StyleName tstyle)
											(cons 'ScaleFactor twidth)))
				xiatext (bf-app-bzb-addtext ypt p2 theight 
									(list 
										(cons 'Layer tlayer)
										(cons 'StyleName tstyle)
										(cons 'ScaleFactor twidth)))
			)
			;;计算文字的包围框
			(vla-GetBoundingBox shangtext 'p3 'p4)
			(vla-GetBoundingBox xiatext 'p5 'p6)
			;;计算文字的最大长度
			(setq xianchang (- 
												(BF-math-maxlist (mapcar 'car (mapcar 'vlax-safearray->list (list p3 p4 p5 p6))))
												(BF-math-minlist (mapcar 'car (mapcar 'vlax-safearray->list (list p3 p4 p5 p6))))))
			
			(setq pt2 (polar pt0 0 xianchang));横线端点 
			(setq hengxian (BF-ent-line pt0 pt2));画横线
			
			(cond ;根据箭头样式画箭头,画引线
				((= "0" arrow)(setq xiexian (BF-Ent-Line pt0 pt0)));无箭头
				((= "1" arrow) (setq shizi (bf-app-bzb-shizi pt0 theight))(setq xiexian (BF-Ent-Line pt0 pt0)));十字
				((= "2" arrow) (setq xiexian (bf-app-bzb-arrow pt0 pt0 theight)));箭头
				((= "3" arrow) (setq shizi (bf-app-bzb-dot pt0 theight))(setq xiexian (BF-Ent-Line pt0 pt0)));圆点
			)
			(setq gr 0 gr-model 0 gr-value 0 );;gr-model必须归零
			(while (/= gr-model 3) ;鼠标左键，确认标注位置，退出循环
				(setq gr (grread T 8)   
					gr-model (car gr)   
					gr-value (cadr gr);鼠标位置
				)      
				(if  (and gr (=  gr-model 5));鼠标移动
					(progn
						
						(if (>= (car gr-value)(car pt0));如果文字点在坐标点右边
							(setq 
								pt2 (polar gr-value 0 xianchang);横线端点
								p1 (polar gr-value (* pi 0.5) (/ theight 3));上文字位置
								p2 (polar gr-value (* pi  -0.5) (+ theight (/ theight 3))));下文字位置 
							(setq 
								pt2 (polar gr-value pi xianchang)                
								p1 (polar pt2 (* pi 0.5) (/ theight 3))
								p2 (polar pt2 (* pi  -0.5) (+ theight (/ theight 3))));文字点在坐标点左边
						)
						(vla-put-StartPoint hengxian (vlax-3D-point gr-value))
						(vla-put-EndPoint hengxian (vlax-3D-point pt2))
						(vla-put-InsertionPoint shangtext (vlax-3D-point p1))
						(vla-put-InsertionPoint xiatext (vlax-3D-point p2))
						(if (/= "2" arrow)
							(vla-put-EndPoint xiexian (vlax-3D-point gr-value))
							(progn
								(vla-put-Coordinates 
									xiexian 
									(BF-vla-List->Array 
										(apply 'append 
											(mapcar 'BF-point-3d->2d 
												(list pt0 
													(if (< theight (BF-Math-Length pt0 gr-value))
														(polar pt0 (angle pt0 gr-value) theight)
														gr-value
													) 
													gr-value
												)
											)
										)
										5)
								)
							)
						)
					)
				)
			)
			;(BF-Ent-Group 
			;	(append 
			;		(list 
			;			xiexian 
			;			(BF-Ent-Block (list shangtext xiatext hengxian) "*U" gr-value)  
			;		)
			;		(if (and shizi (vl-consp shizi))
			;			shizi
			;			(list shizi)
			;		)
			;	)
			;	"*"
			;)
			(setq shuju (cons (list (substr xpt 3) (substr ypt 3) "0.0") shuju))
			(setvar "OSMODE" 679);;;改变捕捉模式为下次循环作准备
			(bf-app-bzb-sub)
		)
	)
)



;;;======================================
;;;===========以下为内裤部分=============
;;;======================================
(defun BF-dcl-Init (title focus dclstr / dclfile)
	(setq dclfile (vl-filename-mktemp "DclTemp.dcl"))
	(BF-dcl-PrintDcl 
		(BF-dcl-setDialog
			(strcat (vl-filename-base dclfile) ":dialog")
			(list (cons 'label title) 
				(cons 'initial_focus focus))
			dclstr
		)
		dclfile
	)
	(BF-return (cons dclfile (BF-dcl-getId dclfile)))
)
(defun BF-dcl-setLayout (layoutname layoutlst lst)
	(list (strcat ":" layoutname) (append (BF-dcl-listsplit layoutlst) (apply 'append lst)))
)
(defun BF-dcl-addItem (itemname lst)
	(list (strcat ":" itemname) (BF-dcl-listsplit lst))
)
(defun BF-dcl-getValues (lst)
	(cond
		((listp lst)(mapcar 'get_tile lst))
		((= (type lst) 'str) (get_tile lst))
		(t nil)
	)
)
(defun BF-dcl-Start (dcl-id actionlist)
	(if (not (new_dialog (vl-filename-base (car dcl-id)) (cdr dcl-id)))
		(exit)
	)
	(if (listp actionlist) 
		(mapcar 
			'(lambda (x / tmpx tmpy)
				 (setq tmpx (car x))
				 (setq tmpy (cdr x))
				 (cond 
					 ((and tmpx (= tmpx "set") tmpy) (BF-dcl-setValues tmpy))
					 ((and tmpx (= tmpx "mode") tmpy) (BF-dcl-setModes tmpy))
					 ((and tmpx (= tmpx "action") tmpy) (BF-dcl-setAction tmpy))
					 ((and tmpx (= tmpx "list") tmpy) 
						 (mapcar '(lambda (y) (apply 'BF-dcl-addlist y)) tmpy))
					 ((and tmpx (= tmpx "image") tmpy) 
						 (mapcar '(lambda (y)(apply 'BF-dcl-loadsld y)) tmpy))
				 )
			 )
			actionlist
		)
	)
	(BF-return (start_dialog))
)
(defun BF-math-rtos (real prec / dimzin result)
	(setq dimzin (getvar 'dimzin))
	(setvar 'dimzin 0)
	(setq result (vl-catch-all-apply 'rtos (list real 2 prec)))
	(setvar 'dimzin dimzin)
	(if (not (vl-catch-all-error-p result))
		result
	)
)
(defun BF-ent-Layers ()
	; (setq layers-obj (vla-get-Layers doc))
	; (setq layer-list '())
	;(vlax-for i layers-obj
	;	(setq layer-list (append layer-list (list (vla-get-Name i))))
	;)
	;layer-list
  (BF-Ent-ListCollection (BF-Layers))
)
(defun BF-Ent-TextStyles ()
  (BF-Ent-ListCollection (BF-TextStyles))
)
(defun BF-dcl-setModes (lst)
	(mapcar 'mode_tile (mapcar 'car lst) (mapcar 
																				 '(lambda (x) 
																						(if (and (listp x)(= 'sym (type (car x))))
																							(eval x)
																							x
																						))
																				 (mapcar 'cdr lst)))
)
(defun BF-dcl-setValues (lst)
	(mapcar 
		'set_tile 
		(mapcar 'car lst) 
		(mapcar 
			'(lambda (x / tmp) 
				 (if (and (listp x)(= 'sym (type (car x))))
					 (setq tmp (eval x))
					 x
				 ))
			(mapcar 'cdr lst)))
)
(defun BF-AssocList-AppendList (lst value)
	(if (listp value)
		(foreach item value
			(setq lst (BF-AssocList-AppendItem lst item))
		)
	)
)
(defun BF-AssocList-AppendItem (lst value)
	(if (assoc (car value) lst)
		(setq lst (BF-AssocList-Remove lst (car value)))
	)
	(cons value lst)
)
(defun BF-dcl-End (dclid)
	(unload_dialog (cdr dclid))
	(vl-file-delete (car dclid))
	(setq *user-dclfile-dclid-list* (vl-remove dclid *user-dclfile-dclid-list*))
	(princ)
)
(defun BF-startundo (doc)
	(BF-endundo doc)
	(vla-startundomark doc)
)

;;;函数名称:BF-AssocList-Key
;;;函数说明:返回关联表中key对应的value
;;;参    数:lst:关联表
;;;参    数:key:关键字
;;;返 回 值:key对应的value
;;;示    例:(BF-AssocList-Key lst key)
(defun BF-AssocList-Key (lst key)
	(cdr (assoc key lst))
)
(defun BF-active-document nil 
	(eval (list 'defun 'BF-active-document 'nil (vla-get-activedocument (vlax-get-acad-object))))
	(BF-active-document)
)
(defun BF-endundo ( doc )
	(while (= 8 (logand 8 (getvar 'undoctl)))
		(vla-endundomark doc)
	)
)
(defun BF-model-space nil
	(eval (list 'defun 'BF-model-space 'nil (vla-get-modelspace (vla-get-activedocument (vlax-get-acad-object)))))
	(BF-model-space)
)
(defun BF-Ent-Line (start end)
	(vla-AddLine (BF-model-space) (vlax-3D-point start) (vlax-3D-point end))
)
(defun BF-Math-Length (start end)
	(BF-Vec-Norm (mapcar '- end start))
)
(defun BF-vla-List->Array (nList arraytype)
  (vlax-SafeArray-Fill
    (vlax-Make-SafeArray
      arraytype
      (cons 0 (1- (length nList)))
    )
    nList
  )
)
(defun BF-point-3d->2d (3dpt)
  (if (listp 3dpt)
    (list (float (car 3dpt)) (float (cadr 3dpt)))
  )
)
(defun BF-math-TransPt (base usrpt transpt ang)
	(car (BF-Mat-RotateByMatrix 
				 (BF-Mat-TranslateByMatrix (list transpt) base usrpt)
				 usrpt (- ang)))
)
(defun BF-math-maxlist (lst) 
	(if (atom lst)
		lst
		(apply 'max lst))
)
(defun BF-math-minlist (lst) 
	(if (atom lst)
		lst
		(apply 'min lst))
)
(defun BF-dcl-PrintDcl	(x f / _PrintItem _PrintList _Main file)
	
  (defun _PrintItem (_PrintMethod item indents)
    (cond
      (item
				(princ "\n" file)
				(repeat indents (princ "    " file))
				(_PrintMethod item file)
      )
      ((princ " { }" file))
    )
    (princ)
  )
	
  (defun _PrintList (_PrintMethod lst indents)
    (if	(< -1 indents)
      (_PrintItem _PrintMethod "{" indents)
    )
    ((lambda (i) (foreach x lst (_Main x i))) (1+ indents))
    (if	(< -1 indents)
      (_PrintItem _PrintMethod "}" indents)
      (princ)
    )
  )
	
  (defun _Main (x indents)
    (if	(vl-consp x)
      (if ((lambda (x) (and x (atom x))) (cdr x))
				(_PrintItem princ x indents)
				(_PrintList princ x indents)
      )
      (_PrintItem princ x indents)
    )
  )
	
  (cond
    ((setq file (open f "w"))
			(_Main x -1)
			(close file)
			;(startapp "notepad" f)
    )
  )
)
(defun BF-dcl-setDialog (dialogname layoutlst lst)
	(list dialogname (append (BF-dcl-listsplit layoutlst) (apply 'append lst)))
)
(defun BF-return (value) value)
(defun BF-dcl-getId (dcl_file / dcl_handle)
  (cond
    ;;如果已经打开dcl文件，就返回dcl文件句柄
    ((BF-return (cdr (assoc dcl_file *user-dclfile-dclid-list*))))
    ;; 如果没有找到dcl文件，就弹出对话框，并返回nil
    ((not (findfile dcl_file))
      (alert
        (strcat
          "未找到对话框定义文件 " dcl_file
          ".dcl\n请检查支持目录。"))
      (BF-return nil)
    )
		
    ;; 找到文件，如果加载不成功，弹出对话框，返回nil
    ((or (not (setq dcl_handle (load_dialog dcl_file)))
			 (> 1 dcl_handle))
      (alert
        (strcat
          "无法加载对话框控制文件 " dcl_file 
          "\n请检查支持目录。"))
      (BF-return nil)
    )
		
    ;; 找到文件，并且加载成功，将文件名和句柄组成点对标存放在常量中，并返回句柄
    (t (setq *user-dclfile-dclid-list* 
				 (BF-AssocList-AddItem  *user-dclfile-dclid-list* (cons dcl_file dcl_handle)))
      (BF-return dcl_handle)
    )
  )
)
(defun BF-dcl-listsplit (lst)
	(if (listp lst)
		(mapcar
			'(lambda (x / tmp)
				 (strcat 
					 (strcase (vl-princ-to-string (car x)) t) 
					 " = " 
					 (if (BF-stringp (setq tmp (cdr x)))
						 (vl-prin1-to-string tmp)
						 (strcase (vl-prin1-to-string tmp) t)
					 )
					 ";"))
			lst))
	
)
(defun BF-dcl-setAction (lst)
	(if (listp lst)
		;(mapcar 'action_tile (mapcar 'car lst) (mapcar 'cadr lst))
		(mapcar 
			'(lambda (x)
				 (BF-CatchApply 
					 'action_tile 
					 (cons 
						 (car x) 
						 (list 
							 (apply 
								 'strcat 
								 (mapcar 'vl-prin1-to-string (cdr x))))))) 
			lst)
	)
)
(defun BF-dcl-addlist (key lst)
	;(cond
	;	((and (null operation) (null index))
	;		(start_list key)
	;	)
	;	((and (null index) operation)
	;		(start_list key operation)
	;	)
	;	(t (start_list key operation index))
	;)
	
	(if (= 'sym (type (car lst)))
		(setq lst (eval lst))
	)
	
	(start_list key)
	(mapcar 'add_list (mapcar 'vl-princ-to-string lst))
	(end_list)
)
(defun BF-dcl-loadsld (key sld / x y)
	(setq x (dimx_tile key))
	(setq y (dimy_tile key))
	(start_image key)
	(cond((numberp sld) (fill_image 0 0 x y sld))
		(t
			(fill_image 0 0 x y -2)
			(slide_image 0 0 x y sld)
		)
	)
	(end_image)
)
(defun BF-Ent-ListCollection (collection / out)
  (vlax-for each collection
    (setq out (cons (vla-get-Name each) out))
  )
  (reverse out)
)
(defun BF-Layers nil
	(eval (list 'defun 'BF-Layers 'nil (vla-get-Layers (vla-get-activedocument (vlax-get-acad-object)))))
	(BF-Layers)
)
(defun BF-TextStyles ()
	(eval (list 'defun 'BF-TextStyles 'nil (vla-get-TextStyles (vla-get-activedocument (vlax-get-acad-object)))))
	(BF-TextStyles)
)
(defun BF-AssocList-Remove (lst key)
  (vl-remove-if '(lambda (x) (equal (car x) key)) lst)
)
(defun BF-Vec-Norm (v)
  (sqrt (apply '+ (mapcar '* v v)))
)
(defun BF-Mat-RotateByMatrix (target p1 ang / m)
  (BF-Mat-ApplyMatrixTransformation target
    (setq m
      (list
        (list (cos ang) (- (sin ang)) 0.)
        (list (sin ang) (cos ang) 0.)
        (list 0. 0. 1.)
      )
    )
    (mapcar '- p1 (BF-Mat-MxV m p1))
  )
)
(defun BF-Mat-TranslateByMatrix (target p1 p2)
  (BF-Mat-ApplyMatrixTransformation target
    (list
      (list 1. 0. 0.)
      (list 0. 1. 0.)
      (list 0. 0. 1.)
    )
    (mapcar '- p2 p1)
  )
)
(defun BF-AssocList-AddItem (lst value)
	(if (assoc (car value) lst)
		lst
		(cons value lst)
	)
)
(defun BF-stringp (arg) (equal (type arg) 'str))
(defun BF-CatchApply (fun args / result)
  (if
    (not
      (vl-catch-all-error-p
				(setq result
					(vl-catch-all-apply
						(if (= 'SYM (type fun))
							fun
							(function fun)
						)
						args
					)
				)
      )
    )
		result
  )
)
(defun BF-Mat-ApplyMatrixTransformation (target matrix vector) 
  (cond
    ((eq 'VLA-OBJECT (type target))
      (vla-TransformBy target
        (vlax-tMatrix
          (append (mapcar (function (lambda (x v) (append x (list v)))) matrix vector)
						'((0. 0. 0. 1.))
          )
        )
      )
    )
    ((listp target)
      (mapcar
        (function
          (lambda (point) (mapcar '+ (BF-Mat-MxV matrix point) vector))
        )
        target
      )
    )        
  )
)
(defun BF-Mat-MxV (m v)
  (mapcar (function (lambda (r) (apply '+ (mapcar '* r v)))) m)
)



(Defun BF-Excel-New (ishide / Rtn)
  (if (setq Rtn (vlax-get-or-create-object "Excel.Application"))
    (progn
      (vlax-invoke
				(vlax-get-property Rtn 'WorkBooks)
				'Add
      )
      (if ishide
				(vla-put-visible Rtn 1)
				(vla-put-visible Rtn 0)
      )
    )
  )
  Rtn
)
(defun BF-Excel-MergeRange (xlapp index / range)
  (vlax-invoke-method
    (BF-Excel-getRange
      xlapp
      (BF-Excel-Utils-index-cells->range index)
    )
    'Merge
  )
  (BF-Excel-getRange XLApp index)
)
(Defun BF-Excel-setRangeValue (XLApp index value / range)
	
  (setq range (BF-Excel-getRange XLApp index))
  (if (= 'list (type value))
    (progn
      (vlax-for	it range
				(vlax-put-property it 'value2 (car value))
				(setq value (cdr value))
      )
    )
    (progn
      (vlax-for	it range
				(vlax-put-property it 'value2 value)
      )
    )
  )
)
(defun BF-Excel-getRange (xlapp index)
  (vlax-get-property
    (vlax-get-property
      (vlax-get-property xlapp 'ActiveWorkbook)
      'ActiveSheet
    )
    'range
    (BF-Excel-Utils-index-cells->range index)
  )
)
(defun BF-Excel-Utils-index-cells->range (lst / num->col)
  (defun num->col (n)			;数字转为列，leemac
    (if	(< n 27)
      (chr (+ 64 n))
      (strcat (num->col (/ (1- n) 26))
	      (num->col (1+ (rem (1- n) 26)))
      )
    )
  )
  (if (= 'list (type lst))
    (cond
      ((= 2 (length lst))
				(strcat (num->col (cadr lst)) (itoa (car lst)))
      )
      ((= 4 (length lst))
				(strcat (num->col (cadr lst))
					(itoa (car lst))
					":"
					(num->col (last lst))
					(itoa (caddr lst))
				)
      )
      (t
				"A1"
      )
    )
    lst
  )
)

(print "欢迎使用坐标标注工具，命令：bzb")
(princ)