
;;;name:BF-vla-getvalue
;;;desc:变体里取值
;;;arg:var:变体或者数组
;;;return:数据列表
;;;example:(BF-vla-getvalue	var)
(defun BF-Vla-GetValue (var)
  (cond
    ((listp var)
     (mapcar 'BF-vla-getvalue var)
    )
    ((BF-variantp var)
     (BF-vla-getvalue (vlax-variant-value var))
    )
    ((BF-safearrayp var)
     (mapcar 'BF-vla-getvalue (vlax-safearray->list var))
    )
    (T var)
  )
)

;;;name:BF-vla-List->Array
;;;desc:表->安全数组类型（一维数组）
;;;arg:nlist:列表，要求数据的类型要和arraytype一致
;;;arg:arraytype:可指定如下常量：可以用后面的数字也可以用前面的类型符号
;;;vlax-vbInteger (2) 整数型
;;;vlax-vbLong (3) 长整数型
;;;vlax-vbSingle (4) 单精度浮点数
;;;vlax-vbDouble (5) 双精度浮点数
;;;vlax-vbString (8) 字符串
;;;vlax-vbObject (9) 对象
;;;vlax-vbBoolean (11) 布尔值
;;;vlax-vbVariant (12) 变体
;;;return:一维数组
;;;example:(BF-vla-List->Array '(1 2 3 4) vlax-vbInteger)
(defun BF-vla-List->Array (nList arraytype)
  (vlax-SafeArray-Fill
    (vlax-Make-SafeArray
      arraytype
      (cons 0 (1- (length nList)))
    )
    nList
  )
)

;;;name:BF-vla-list->arrays
;;;desc:表->安全数组类型（多维数组，多于二维报错）
;;;arg:nlist:列表，要求数据的类型要和arraytype一致，表的各维必须为表
;;;arg:arraytype:可指定如下常量：可以用后面的数字也可以用前面的类型符号
;;;vlax-vbInteger (2) 整数型
;;;vlax-vbLong (3) 长整数型
;;;vlax-vbSingle (4) 单精度浮点数
;;;vlax-vbDouble (5) 双精度浮点数
;;;vlax-vbString (8) 字符串
;;;vlax-vbObject (9) 对象
;;;vlax-vbBoolean (11) 布尔值
;;;vlax-vbVariant (12) 变体
;;;return:多维数组
;;;example:(BF-vla-List->Arrays '((1 2) (3 4)) vlax-vbInteger)
(defun BF-vla-list->arrays (nlist arraytype)
					;(setq nlist (BF-indot->list nlist))
  (vlax-SafeArray-Fill
    (apply 'vlax-Make-SafeArray
	   (cons arraytype
		 (mapcar
		   '(lambda (x)
		      (cons 0 (1- (length x)))
		    )
		   nlist
		 )
	   )
    )
    nlist
  )
)

;;;函数名称:BF-vla-ObjArray
;;;函数说明:创建vla对象数组
;;;参    数:lst:vla对象表
;;;返 回 值:vla对象数组
;;;示    例:(BF-vla-ObjArray lst)
(defun BF-vla-ObjArray (lst) (BF-vla-List->Array lst vlax-vbObject))

;;;name:BF-vla-ObjectVariant
;;;desc:创建vla对象表变体
;;;arg:lst:vla对象表
;;;return:变体
;;;example:(BF-vla-ObjectVariant lst)
(defun BF-vla-ObjectVariant (lst)
  (vlax-make-variant
    (vlax-safearray-fill
      (vlax-make-safearray
	vlax-vbobject
	(cons 0 (1- (length lst)))
      )
      lst
    )
  )
)


;;;name:BF-enamelist->vla
;;;desc:图元列表转为Vla列表
;;;arg:lst:图元列表
;;;return:Vla列表
;;;example:(BF-enamelist->vla lst)
(defun BF-enamelist->vla (lst)
  (mapcar 'vlax-ename->vla-object lst)
)


;;;name:BF-Vla-BuildFilter
;;;desc:构建variant列表
;;;arg:filter:点对列表
;;;return:variant列表
;;;example:(BF-Vla-BuildFilter '((1 . "123")(2 . "4556")))
(defun BF-Vla-BuildFilter (filter)
  (vl-load-com)
  (mapcar '(lambda (lst typ)
	     (vlax-make-variant
	       (vlax-safearray-fill
		 (vlax-make-safearray
		   typ
		   (cons 0
			 (1- (length lst))
		   )
		 )
		 lst
	       )
	     )
	   )
	  (list (mapcar 'car filter) (mapcar 'cdr filter))
	  (list vlax-vbInteger vlax-vbVariant)
  )
)
