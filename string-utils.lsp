
;;;name:BF-string-add
;;;desc:字符串相加
;;;arg:strlst:字符串或字符串列表
;;;return:返回组合成的字符串，参数不符合要求，则返回nil
;;;example:(BF-string-add '("a" "b" "c"))
(defun BF-str-add (strlst)
	(cond 
		((BF-stringp strlst) strlst) 
		((BF-string-listp strlst) (apply 'strcat strlst))
		(t nil)
	)
)

;;;name:BF-string-square
;;;desc:字符串自乘
;;;arg:int:数字
;;;arg:str:字符串
;;;return:返回字符串自乘int次的字符串
;;;example:(BF-string-square 2 "a")
(defun BF-str-square	(int str)
  (if (zerop int)
    str
    (strcat str (BF-str-square (1- int) str))
  )
)
;;;name:BF-lst->str
;;;desc:列表转成字符串--leemac
;;;arg:lst:字符串列表
;;;arg:del:分隔符
;;;return:字符串
;;;example:(BF-lst->str '("a" "b" "c") "-")
(defun BF-lst->str	(lst del)
	(if	(cdr lst)
		(strcat (car lst) del (BF-lst->str (cdr lst) del))
		(car lst)
	)
)

;;;name:BF-str->lst
;;;desc:字符串转成列表--leemac
;;;arg:str:字符串
;;;arg:del:分隔符
;;;return:字符串列表
;;;example:(BF-str->lst "a-b-c" "-")
(defun BF-str->lst	(str del / pos)
	(if	(setq pos (vl-string-search del str))
		(cons (substr str 1 pos)
			(BF-str->lst (substr str (+ pos 1 (strlen del))) del)
		)
		(list str)
	)
)

;;;name:BF-Str-ParseByLst
;;;desc:字符串按分隔符列表转列表
;;;arg:lstr:字符串
;;;arg:DelimLst:分隔符列表
;;;return:字符串列表
;;;example:(BF-Str-ParseByLst "a-b=c" '("-" "="))
(defun BF-Str-ParseByLst (lstr DelimLst)
  (setq lstr (list lstr))
  (foreach del DelimLst
    (setq lstr (apply 'append (mapcar '(lambda (x) (BF-str->lst x del)) lstr)))
	)
  (if (member " " DelimLst)
    (vl-remove "" lstr)
	  lstr
	)
)


;;;name:BF-str-subst-all
;;;desc:在字符串中搜索替换全部子串--leemac
;;;arg:new:要替换的新子串
;;;arg:old:被替换的旧子串
;;;arg:str:要处理的字符串
;;;return:替换后的字符串
;;;example:(BF-str-subst-all "abc" "qwe" "abcpoilde")
(defun BF-str-subst-all (new old str / inc len)
	(setq len (strlen new)
		inc 0
	)
	(while (setq inc (vl-string-search old str inc))
		(setq str (vl-string-subst new old str inc)
			inc (+ inc len)
		)
	)
	str
)


;;;name:BF-str-format
;;;desc:字符串格式化函数
;;;arg:str:需要格式化的字符串
;;;arg:formatlist:格式化内容列表或格式化字符串
;;;return:格式化后的字符串
;;;example:(BF-str-format "a{1}b{0}c" '("qwe" "abcpoilde"))
;;;todo:加上复杂的格式化类型
(defun BF-str-format (str formatlist / i str-length)
	(if (BF-stringp formatlist)
		(setq str (BF-str-subst-all formatlist "{0}" str))
		(progn
			(setq i -1)
			(repeat (vl-list-length formatlist)
				(setq str 
					(BF-str-subst-all 
						(nth (setq i (1+ i)) formatlist)
						(strcat "\{" (itoa i) "\}")
						str)))
			
		)
	)
)

;;;name:BF-str-Numberp
;;;desc:确定字符串是否为数字
;;;arg:str:检查的字符串
;;;return:字符串为数字为T,反之为nil
;;;example:(BF-str-Numberp "+.123") 返回 T\n(BF-str-Numberp '1B5') 返回 nil
(defun BF-str-Numberp (str)
  (numberp 
		(vl-catch-all-apply
			'read
			(list 
				(cond
					((= "." (substr str 1 1))
						(strcat "0" str)
					)
					((and
						 (= "-." (substr str 1 2))
						 (> (strlen str) 2)
					 )
						(strcat "-0" (substr str 2))
					)
					((and
						 (= "+." (substr str 1 2))
						 (> (strlen str) 2)
					 )
						(strcat "+0" (substr str 2))
					)
					(t
						str
					)
				)
			)
		)
  )
)

;;;name:BF-Str-Intp
;;;desc:确定字符串是否为整数
;;;arg:str:检查的字符串
;;;return:字符串为整数为T,反之为nil
;;;example:(BF-Str-Intp "+.123") 返回 nil
(defun BF-Str-Intp (str)
  (and (BF-str-Numberp str)
		(not
			(or
				(= "." (substr str 1 1))
				(= "-." (substr str 1 2))
				(= "+." (substr str 1 2))
			)
		)
		(= 'int (type (read str)))
	)
)

;;;name:BF-Str-Realp
;;;desc:确定字符串是否为实数
;;;arg:str:检查的字符串
;;;return:字符串为实数为T,反之为nil
;;;example:(BF-Str-Realp "+.123") 返回 T
(defun BF-Str-Realp (str)
  (and (BF-str-Numberp str)
		(or
			(= "." (substr str 1 1))
			(= "-." (substr str 1 2))
			(= "+." (substr str 1 2))
			(= 'int (type (read str)))
			(= 'REAL (type (read str)))
		)
	)
)

;;(BF-RegExSearch string Express key) 正则表达式搜索字串
;;参数 string = 字串
;;     Express = 正则表达式
;;     key = 字母 i I m M g G的组合字串
;;     i/I = 忽略大小写 m/M = 多行搜索 g/G = 全文搜索

;;;name:BF-RegExp-Search
;;;desc:正则表达式搜索字串
;;;arg:string:被搜索字符串
;;;arg:express:正则匹配规则
;;;arg:key:字母 i I m M g G的组合字串，i/I = 忽略大小写 m/M = 多行搜索 g/G = 全文搜索
;;;return:匹配到的字符串表 ((起点位置，长度，字符串)(起点位置，长度，字符串)...)
;;;example:(BF-RegExp-Search "12345asd66" "asd" "mg")
(defun BF-RegExp-Search (string express key / regex s pos len str l)
  (setq RegEx (vlax-create-object "Vbscript.RegExp"))
  (if (and key (wcmatch key "*g*,*G*"))
    (vlax-put-property regex "Global" 1)
    (vlax-put-property regex "Global" 0)
  )
  (if (and key (wcmatch key "*i*,*I*"))
    (vlax-put-property regex "IgnoreCase" 1)
    (vlax-put-property regex "IgnoreCase" 0)
  )
  (if (and key (wcmatch key "*m*,*M*"))
    (vlax-put-property regex "Multiline" 1)
    (vlax-put-property regex "Multiline" 0)
  )
  (vlax-put-property regex "Pattern" ExPress)
  (setq s (vlax-invoke-method regex 'Execute string))
  (vlax-for o s
    (setq pos (vlax-get-property o "FirstIndex")
		  len (vlax-get-property o "Length")
		  str (vlax-get-property o "value")
    )
    (setq l (cons (list pos len str) l))
  )
  (vlax-release-object RegEx)
  (reverse l)
)

;;;name:BF-RegExp-RePlace
;;;desc:正则表达式替换字串
;;;arg:string:被替换字符串
;;;arg:newstr:替换的字串
;;;arg:express:正则匹配规则
;;;arg:key:字母 i I m M g G的组合字串，i/I = 忽略大小写 m/M = 多行搜索 g/G = 全文搜索
;;;return:替换后的字符串
;;;example:(BF-RegExp-RePlace "12345asd66" "bvd" "asd" "mg")
(defun BF-RegExp-RePlace (string newstr express key / regex s)
  (setq RegEx (vlax-create-object "Vbscript.RegExp"))
  (if (and key (wcmatch key "*g*,*G*"))
    (vlax-put-property regex "Global" 1)
    (vlax-put-property regex "Global" 0)
  )
  (if (and key (wcmatch key "*i*,*I*"))
    (vlax-put-property regex "IgnoreCase" 1)
    (vlax-put-property regex "IgnoreCase" 0)
  )
  (if (and key (wcmatch key "*m*,*M*"))
    (vlax-put-property regex "Multiline" 1)
    (vlax-put-property regex "Multiline" 0)
  )
  (vlax-put-property regex "Pattern" express)
  (setq s (vlax-invoke-method regex 'RePlace string newstr))
  (vlax-release-object RegEx)
  s
)

;;;name:BF-Str-Reverse
;;;desc:反转字符串
;;;arg:str:字符串
;;;return:反转后的字符串，可能会乱码
;;;example:(BF-Str-Reverse "+.123") 返回 "321.+"
(defun BF-Str-Reverse (str)
(vl-list->string (reverse (vl-string->list str)))
  )

;;;name:BF-Str-RightSubstr
;;;desc:返回字符串中的一个子字符串,开始位置从右开始
;;;arg:str:字符串
;;;arg:start:开始位置，从右开始，从1起始
;;;arg:len:指定在 string 中进行搜索的字符的长度。如果未指定len，则子字符串延续到字符串的开头
;;;return:字符串
;;;example:(BF-Str-RightSubstr "123456789" 2 3) 返回 "678"
(defun BF-Str-RightSubstr (str start len)	
(substr str (- (strlen str) len start -2) len)
)

;;;name:BF-Str-RSubstr
;;;desc:返回字符串从右开始指定长度的一个子字符串
;;;arg:str:字符串
;;;arg:len:指定在 string 中进行搜索的字符的长度。
;;;return:字符串
;;;example:(BF-Str-RSubstr "123456789" 2) 返回 "89"
(defun BF-Str-RSubstr (str len)	
	(substr str (- (strlen str) len -1))
)

;;;name:BF-Str-LSubstr
;;;desc:返回字符串从左开始指定长度的一个子字符串
;;;arg:str:字符串
;;;arg:len:指定在 string 中进行搜索的字符的长度。
;;;return:字符串
;;;example:(BF-Str-LSubstr "123456789" 2) 返回 "12"
(defun BF-Str-LSubstr (str len)
(Substr str 1 len)

)


;;;字符串切换大小写
;;;(bf-str-case "hEllO") ===> "HeLLo"
(defun bf-str-case (a)
  (vl-list->string
    (mapcar '(lambda (x)
               (cond ((and (>= x 65) (< x 97)) (+ x 32))
                     ((and (>= x 97) (< x 123)) (- x 32))
               )
             )
            (vl-string->list a)
    )
  )
)









